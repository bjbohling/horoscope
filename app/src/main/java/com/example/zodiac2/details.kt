package com.example.zodiac2

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class details : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.details)

        val detailsIntent = intent

        val itemName = detailsIntent.getStringExtra("itemName")
        val monthName = detailsIntent.getStringExtra("monthName")
        val symbol = detailsIntent.getStringExtra("symbol")
        val description = detailsIntent.getStringExtra("description")

        val resultTextView = findViewById<TextView>(R.id.textView2)

        resultTextView.text = itemName + "\n\n" +
                itemName + " are considered " + description +
                "\n\n" + symbol +
                "\n\n" + monthName
    }
}