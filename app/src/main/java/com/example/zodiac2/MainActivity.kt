package com.example.zodiac2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    val nameArray = arrayOf("Aries","Taurus","Gemini","Cancer", "Leo",
        "Virgo", "Libra","Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces")

    val monthArray = arrayOf("April", "May", "June", "July" , "August" ,"September" ,
        "October" ,"November" , "December" , "January" , "February" ,"March")

    val symbolArray = arrayOf("Ram", "Bull","Twins", "Crab","Lion", "Virgin","Scales", "Scorpion","Archer", "Goat","Water Bearer", "Fish")

    val descriptionArray = arrayOf("Courageous and Energetic",
        "Known for being reliable, practical, ambitious and sensual.",
        "Gemini-born are clever and intellectual",
        "Tenacious, loyal and sympathetic.",
        "Warm, action-oriented and driven by the desire to be loved and admired",
        "Methodical, meticulous, analytical and mentally astute.",
        "Librans are famous for maintaining balance and harmony",
        "Strong willed and mysterious",
        "Born adventurers",
        "The most determined sign in the Zodiac.",
        "Humanitarians to the core",
        "Proverbial dreamers of the Zodiac.")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val adapter = ArrayAdapter(this,
            R.layout.listviewitem, nameArray)

        val listView:ListView = findViewById(R.id.listView)
        listView.setAdapter(adapter)

        val intent = Intent(this, details::class.java)


        listView.onItemClickListener = object : AdapterView.OnItemClickListener {

            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {


                val itemName = listView.getItemAtPosition(position) as String

                val monthName = monthArray.get(position) as String
                val symbol = symbolArray.get(position) as String
                val description = descriptionArray.get(position) as String


                intent.putExtra("itemName", itemName)
                intent.putExtra("monthName",monthName )
                intent.putExtra("symbol",symbol )
                intent.putExtra("description",description )

                startActivity(intent);

            }
        }

    }
}
